/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.MCarrera;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yas
 */
public class VCarrera extends javax.swing.JFrame {

    MCarrera m_MPlanEstudio;

    /**
     * Creates new form VPlanEstudio
     */
    public VCarrera() {
        m_MPlanEstudio = new MCarrera();
        initComponents();

    }

    public void mostrar() {
        DefaultTableModel m_tableModel = new DefaultTableModel();
        m_tableModel.addColumn("ID");
        m_tableModel.addColumn("TITULO");

        m_tableModel.addColumn("DESCRIPCION");

        m_tableModel.addColumn("FECHA");
        m_tableModel.addColumn("FECHA");

        m_tableModel.addColumn("COSTO");

        m_tableModel.addColumn("ESTADO");

        v_tb_lista.setModel(m_tableModel);
        String[] datos = new String[7];
        ResultSet re = m_MPlanEstudio.listar();
        try {
            while (re.next()) {
                datos[0] = String.valueOf(re.getInt(1));
                datos[1] = re.getString(2);
                datos[2] = re.getString(3);
                datos[3] = re.getString(4);
                datos[4] = re.getString(5);
                datos[5] = re.getString(6);
                if (re.getBoolean(7)) {
                    datos[6] = "disponible";
                } else {
                    datos[6] = "no disponible";
                }

                m_tableModel.addRow(datos);
            }
            v_tb_lista.setModel(m_tableModel);
        } catch (SQLException ex) {
            Logger.getLogger(VDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        v_btn_registrar = new javax.swing.JButton();
        v_txt_descripcion = new javax.swing.JTextField();
        v_btn_editar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        v_btn_eliminar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        v_txt_id = new javax.swing.JTextField();
        v_txt_titulo = new javax.swing.JTextField();
        v_txt_costo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        v_tb_lista = new javax.swing.JTable();
        v_txt_estado = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        v_txt_fecha2 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        v_txt_fecha3 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        v_btn_registrar.setText("REGISTRAR");

        v_btn_editar.setText("EDITAR");
        v_btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_btn_editarActionPerformed(evt);
            }
        });

        jLabel6.setText("costo");

        v_btn_eliminar.setText("ELIMINAR");

        jLabel7.setText("estado");

        v_txt_id.setEditable(false);
        v_txt_id.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_txt_idActionPerformed(evt);
            }
        });

        v_txt_titulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_txt_tituloActionPerformed(evt);
            }
        });

        jLabel2.setText("titulo");

        jLabel4.setText("descripcion");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("GESTIONAR CARRERA");

        v_tb_lista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(v_tb_lista);

        v_txt_estado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "disponible", "no disponible" }));
        v_txt_estado.setToolTipText("");

        jLabel11.setText("fecha inicio");

        v_txt_fecha2.setText("año-mes-dia");
        v_txt_fecha2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_txt_fecha2ActionPerformed(evt);
            }
        });

        jLabel12.setText("fecha fin");

        v_txt_fecha3.setText("año-mes-dia");
        v_txt_fecha3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_txt_fecha3ActionPerformed(evt);
            }
        });

        jLabel5.setText("id");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 554, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(120, 120, 120)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(v_btn_registrar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addComponent(v_btn_editar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(185, 185, 185)
                                        .addComponent(v_btn_eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(79, 79, 79)
                                .addComponent(jLabel1))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(54, 54, 54)
                                        .addComponent(v_txt_costo, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(38, 38, 38)
                                        .addComponent(v_txt_estado, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(7, 7, 7)
                                        .addComponent(jLabel6)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(22, 22, 22)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel4)
                                            .addGap(1, 1, 1))
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel11))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(1, 1, 1)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(v_txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(v_txt_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(v_txt_fecha2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(27, 27, 27)
                                            .addComponent(jLabel12)
                                            .addGap(18, 18, 18)
                                            .addComponent(v_txt_fecha3, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(v_txt_descripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(8, 8, 8)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(v_txt_descripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(v_txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(v_txt_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(v_txt_fecha3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(v_txt_fecha2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(v_txt_costo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(v_txt_estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(v_btn_registrar)
                    .addComponent(v_btn_editar)
                    .addComponent(v_btn_eliminar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(113, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void v_btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_btn_editarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_btn_editarActionPerformed

    private void v_txt_idActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_txt_idActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_txt_idActionPerformed

    private void v_txt_tituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_txt_tituloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_txt_tituloActionPerformed

    private void v_txt_fecha2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_txt_fecha2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_txt_fecha2ActionPerformed

    private void v_txt_fecha3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_txt_fecha3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_txt_fecha3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JButton v_btn_editar;
    public javax.swing.JButton v_btn_eliminar;
    public javax.swing.JButton v_btn_registrar;
    public javax.swing.JTable v_tb_lista;
    public javax.swing.JTextField v_txt_costo;
    public javax.swing.JTextField v_txt_descripcion;
    public javax.swing.JComboBox v_txt_estado;
    public javax.swing.JTextField v_txt_fecha2;
    public javax.swing.JTextField v_txt_fecha3;
    public javax.swing.JTextField v_txt_id;
    public javax.swing.JTextField v_txt_titulo;
    // End of variables declaration//GEN-END:variables
}
