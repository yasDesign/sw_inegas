package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yas
 * @version 1.0
 * @created 24-abr-2019 06:48:00 a.m.
 */
public class MDetalleModulo {

    public Conexion m_Conexion;
    int idModulo;
    int idInscripcionModulo;

    public MDetalleModulo() {
        m_Conexion = new Conexion();
    }

    public int getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(int idModulo) {
        this.idModulo = idModulo;
    }

    public int getIdInscripcionModulo() {
        return idInscripcionModulo;
    }

    public void setIdInscripcionModulo(int idInscripcionModulo) {
        this.idInscripcionModulo = idInscripcionModulo;
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO detalleModulo(idInscricpionModulo,idModulo)" + "VALUES (?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getIdInscripcionModulo());
            ps.setInt(2, getIdModulo());
            ps.execute();
            System.out.println(MDocente.class.getName() + " registro exitoso de modulo detalle");
            ps.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("erroo cath ...registrar modeulodetalle...");
        }
        return false;
    }

    public boolean editar() {
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM detalleModulo WHERE idInscricpionModulo=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getIdInscripcionModulo());
            ps.execute();
            System.out.println(MDetalleCarrera.class.getName() + "eliminar detalle de modulo");
            ps.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("erroo cath ...elimianr modelo detalle..." + ex);
        }
        return false;
    }

    public ResultSet listar() {
        return null;
    }
}//end MDetalleModulo
