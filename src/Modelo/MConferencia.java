package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 05:34:52 a.m.
 */
public class MConferencia {

    private int id;
    private String titulo;
    private String descripcion;
    private String expositor;
    private String infoExpositor;
    private String fecha;
    private String hora;

    public Conexion m_Conexion;

    public MConferencia() {
        m_Conexion = new Conexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExpositor() {
        return expositor;
    }

    public void setExpositor(String expositor) {
        this.expositor = expositor;
    }

    public String getInfoExpositor() {
        return infoExpositor;
    }

    public void setInfoExpositor(String infoExpositor) {
        this.infoExpositor = infoExpositor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO conferencia(titulo,descripcion,expositor,infoExpositor,fecha,hora)" + "VALUES (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getTitulo());
            ps.setString(2, getDescripcion());
            ps.setString(3, getExpositor());
            ps.setString(4, getInfoExpositor());
            ps.setDate(5,Date.valueOf(getFecha()));
            ps.setTime(6,Time.valueOf(getHora()));
            ps.execute();
            System.out.println(MConferencia.class.getName() + " registro exitoso de conferencia");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM conferencia where id=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.execute();
            System.out.println(MConferencia.class.getName() + " eliminado exitoso de conferencia");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean editar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "UPDATE conferencia SET titulo=?, descripcion=?, expositor=?, infoExpositor=?, fecha=?, hora=?  WHERE id=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getTitulo());
            ps.setString(2, getDescripcion());
            ps.setString(3, getExpositor());
            ps.setString(4, getInfoExpositor());
            ps.setDate(5,Date.valueOf(getFecha()));
            ps.setTime(6,Time.valueOf(getHora()));;
            ps.setInt(7, getId());
            ps.execute();
            System.out.println(MConferencia.class.getName() + " edicion exitoso de conferencia");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM conferencia";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MConferencia.class.getName() + " edicion exitoso de conferencia");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");

        return res;
    }
}//end MConferencia
