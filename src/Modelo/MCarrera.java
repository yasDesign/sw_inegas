package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 05:34:29 a.m.
 */
public class MCarrera {

    private int id;
    private String titulo;
    private String descripcion;
    private String fechaInicio;
    private String fechaFin;
    private float costo;
    private boolean estado;
    public Conexion m_Conexion;

    public MCarrera() {
        m_Conexion = new Conexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO carrera(titulo,descripcion,costo,estado,fechaInicio,fechaFin)" + "VALUES (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getTitulo());
            ps.setString(2, getDescripcion());
            ps.setFloat(3, getCosto());
            ps.setBoolean(4, getEstado());           
            ps.setDate(5, Date.valueOf(getFechaInicio()));
            ps.setDate(6, Date.valueOf(getFechaFin()));

            ps.execute();
            System.out.println(MDocente.class.getName() + " registro exitoso de carrera");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM carrera where id=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.execute();
            System.out.println(MDocente.class.getName() + " eliminado exitoso de carrera");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean editar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "UPDATE carrera SET titulo=?, descripcion=?, costo=?, estado=?,fechaInicio=?,fechaFin=?  WHERE id=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getTitulo());
            ps.setString(2, getDescripcion());
            
            ps.setFloat(3, getCosto());
            ps.setBoolean(4, getEstado());
            ps.setDate(5, Date.valueOf(getFechaInicio()));
            ps.setDate(6, Date.valueOf(getFechaFin()));
            ps.setInt(7, getId());
            ps.execute();
            System.out.println(MDocente.class.getName() + " edicion exitoso de carrera");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM carrera";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MDocente.class.getName() + " listado exitoso de carrera");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");

        return res;
    }
}//end MPlanEstudio
