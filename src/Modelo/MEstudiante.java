package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 05:34:41 a.m.
 */
public class MEstudiante {

    private int id;
    private String nombre;
    private String correo;
    private String profesion;
    private String fechaNac;
    private int telefono;
    private int ci;
    public Conexion m_Conexion;

    public MEstudiante() {
        m_Conexion = new Conexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO estudiante(nombre,correo,ci,telefono,profesion,fechaNac)" + "VALUES (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getNombre());
            ps.setString(2, getCorreo());
            ps.setInt(3, getCi());
            ps.setInt(4, getTelefono());
            ps.setString(5, getProfesion());
            ps.setDate(6, Date.valueOf(getFechaNac()));

            ps.execute();
            System.out.println(MDocente.class.getName() + " registro exitoso de estudiante");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM estudiante where id=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.execute();
            System.out.println(MDocente.class.getName() + " eliminado exitoso de estudiante");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean editar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "UPDATE estudiante SET nombre=?,  profesion=?, ci=?, telefono=?, correo=?, fechaNac=? WHERE id=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getNombre());
            ps.setString(2, getProfesion());
            ps.setInt(3, getCi());
            ps.setInt(4, getTelefono());
            ps.setString(5, getCorreo());
            ps.setDate(6, Date.valueOf(getFechaNac()));
            ps.setInt(7, getId());
            ps.execute();
            System.out.println(MDocente.class.getName() + " edicion exitoso de estudiante");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM estudiante";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MDocente.class.getName() + " edicion exitoso de estudiante");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");

        return res;
    }

}//end MEstudiante
