package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;


/**
 * @author yas
 * @version 1.0
 * @created 21-abr-2019 10:10:14 p.m.
 */
public class MInscribirCarrera {

    private int id;
    private int codigo;
    private String fecha;
    private int idEstudiante;
    public Conexion m_Conexion;

    public MInscribirCarrera() {
        m_Conexion = new Conexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public int registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO inscribirCarrera(codigo,idEstudiante,fecha)" + "VALUES (?,?,?)";
        int id = 0;
        try {
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, getCodigo());
            ps.setInt(2, getIdEstudiante());
            ps.setDate(3,Date.valueOf( LocalDate.now()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            System.out.println(MDocente.class.getName() + " registro exitoso de inscribirCarrera");
        } catch (SQLException ex) {
            System.out.println("erroo cath ...registrar modelo...");
        }
        return id;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM inscribirCarrera WHERE id=?";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.execute();
            System.out.println(MInscribirCarrera.class.getName() + " eliminacion exitoso de inscribirCarrera");
            return true;
        } catch (SQLException ex) {
            System.out.println("erroo cath de inscironvei ..eliminar modelo...");
        }
        return false;
    }

    public boolean editar() {
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM inscribirCarrera";
        try {
            ps = con.prepareStatement(sql);
            ResultSet res = ps.executeQuery();
            System.out.println(MDocente.class.getName() + " listar exitoso de inscribirCarrera");
            return res;
        } catch (SQLException ex) {
            System.out.println("erroo cath ...listar modelo... " + ex);
        }
        return null;
    }
}//end MInscripcionPlanEstudio
