package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 24-abr-2019 06:47:57 a.m.
 */
public class MInscribirModulo {

    public Conexion m_Conexion;
    public String fecha;
    public int codigo;
    public int id;
    public int idEstudiante;

    public MInscribirModulo() {
        m_Conexion = new Conexion();
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public int registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO inscribirModulo(codigo,idEstudiante,fecha)" + "VALUES (?,?,?)";
        int id = 0;
        try {
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, getCodigo());
            ps.setInt(2, getIdEstudiante());
            ps.setDate(3,Date.valueOf( LocalDate.now()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            ps.close();
            System.out.println(MDocente.class.getName() + " registro exitoso de inscribirModulo");
        } catch (SQLException ex) {
            System.out.println("erroo cath ...registrar modelo...");
        }
        return id;
    }

    public boolean eliminar() {
         PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM inscribirModulo WHERE id=?";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.execute();
            System.out.println(MInscribirModulo.class.getName() + " eliminacion exitoso de inscribirmodulo");
            return true;
        } catch (SQLException ex) {
            System.out.println("erroo cath de inscironvei ..eliminar modelo...");
        }
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM inscribirModulo";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MModulo.class.getName() + " listado exitoso de modulo");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MModulo.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MModulo.class.getName() + " ocurrio un fallo");

        return res;
    }

    public boolean editar() {
        return false;
    }

}//end MInscribirModulo
