package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 24-abr-2019 01:47:34 a.m.
 */
public class MModulo {

    private int id;
    private String titulo;
    private String descripcion;
    private int idCarrera;
    private int idDocente;
    
    public Conexion m_Conexion;

    public MModulo() {
        m_Conexion = new Conexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public int getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(int idDocente) {
        this.idDocente = idDocente;
    }

    public Conexion getM_Conexion() {
        return m_Conexion;
    }

    public void setM_Conexion(Conexion m_Conexion) {
        this.m_Conexion = m_Conexion;
    }
    

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO modulo(titulo,descripcion,idDocente,idCarrera)" + "VALUES (?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getTitulo());
            ps.setString(2, getDescripcion());
            ps.setInt(3,getIdDocente());
            ps.setInt(4,getIdCarrera());
            ps.execute();
            System.out.println(MConferencia.class.getName() + " registro exitoso de modulo...");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM  modulo WHERE id=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1,getId());
            ps.execute();
            System.out.println(MConferencia.class.getName() + " eliminarod exitoso de modulo...");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM modulo";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MModulo.class.getName() + " listado exitoso de modulo");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MModulo.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MModulo.class.getName() + " ocurrio un fallo");

        return res;
    }

    public boolean editar() {
        return false;
    }
}//end MModulo
