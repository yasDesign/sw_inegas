package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 04:54:13 a.m.
 */
public class Conexion {

    private String base_db = "/sistemaescuela";
    private String user_db = "root";
    private String pass_db = "";
    private String port_db = "3306";
    private String url_db = "jdbc:mysql://localhost:" + port_db + base_db;
    private Connection con = null;

    public Conexion() {
    }

    public Connection getConexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url_db, user_db, pass_db);

            System.out.println(Conexion.class.getName() + " conexion exitosa");
        } catch (SQLException e) {
            System.out.println(Conexion.class.getName() + " error en la conexion por 1 catch " + e);
        } catch (ClassNotFoundException ex) {
            System.out.println(Conexion.class.getName() + " error en la conexion por 2 catch " + ex);
        }
        return con;
    }

}//end Conexion
