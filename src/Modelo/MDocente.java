package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 04:54:09 a.m.
 */
public class MDocente {

    private int codigo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    private String nombre;
    private String correo;
    private int ci;
    private int telefono;
    private String fechaNac;
    private String direccion;
    private String profesion;
    private int id;
    public Conexion m_Conexion;

    public MDocente() {
        m_Conexion = new Conexion();
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO docente(nombre,ci,profesion,direccion,telefono,correo,fechaNac)" + "VALUES (?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getNombre());
            ps.setInt(2, getCi());
            ps.setString(3, getProfesion());
            ps.setString(4, getDireccion());
            ps.setInt(5, getTelefono());
            ps.setString(6, getCorreo());
            ps.setDate(7, Date.valueOf(getFechaNac()));
            ps.execute();
            System.out.println(MDocente.class.getName() + " registro exitoso de docente");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM docente where id=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.execute();
            System.out.println(MDocente.class.getName() + " eliminado exitoso de docente");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean editar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "UPDATE docente SET nombre=?, ci=?, profesion=?, direccion=?, telefono=?, correo=?,fechaNac=? WHERE id=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getNombre());
            ps.setInt(2, getCi());
            ps.setString(3, getProfesion());
            ps.setString(4, getDireccion());
            ps.setInt(5, getTelefono());
            ps.setString(6, getCorreo());
            ps.setDate(7,Date.valueOf(getFechaNac()));
            ps.setInt(8, getId());
            ps.execute();
            System.out.println(MDocente.class.getName() + " edicion exitoso de docente");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM docente";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MDocente.class.getName() + " edicion exitoso de docente");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MDocente.class.getName() + " ocurrio un fallo");

        return res;
    }
}//end MDocente
