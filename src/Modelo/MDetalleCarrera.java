package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author yas
 * @version 1.0
 * @created 21-abr-2019 10:09:54 p.m.
 */
public class MDetalleCarrera {

    private int idInscripcionCarrera;
    private int idCarrera;
    public Conexion m_Conexion;

    public MDetalleCarrera() {
        m_Conexion = new Conexion();
    }

    public int getIdInscripcionCarrera() {
        return idInscripcionCarrera;
    }

    public void setIdInscripcionCarrera(int idInscripcionCarrera) {
        this.idInscripcionCarrera = idInscripcionCarrera;
    }

    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO detalleCarrera(idInscripcionCarrera,idCarrera)" + "VALUES (?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getIdInscripcionCarrera());
            ps.setInt(2, getIdCarrera());
            ps.execute();
            System.out.println(MDocente.class.getName() + " registro exitoso de Carrera");
            ps.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("erroo cath ...registrar modelo...");
        }
        return false;
    }

    public boolean editar() {
        return false;
    }

    public boolean listar() {
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM detalleCarrera WHERE idInscripcionCarrera=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getIdInscripcionCarrera());
            ps.execute();
            System.out.println(MDetalleCarrera.class.getName() + "eliminar detalle de Carrera");
            ps.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("erroo cath ...elimianr modelo detalle..." + ex);
        }
        return false;
    }
}//end MDetalleCarrera
