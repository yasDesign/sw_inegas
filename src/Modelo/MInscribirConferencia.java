package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author yas
 * @version 1.0
 * @created 21-abr-2019 03:26:50 p.m.
 */
public class MInscribirConferencia {

    private int idConferencia;
    private int idEstudiante;
    private String fecha;
    private boolean estado;
    public Conexion m_Conexion;

    public MInscribirConferencia() {
        m_Conexion = new Conexion();
    }

    public int getIdConferencia() {
        return idConferencia;
    }

    public void setIdConferencia(int idConferencia) {
        this.idConferencia = idConferencia;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean editar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "UPDATE inscribirconferencia SET estado=?, fecha=? WHERE idConferencia=? and idEstudiante=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setBoolean(1, getEstado());
            ps.setDate(4, Date.valueOf(LocalDate.now()));
            ps.setInt(3, getIdConferencia());
            ps.setInt(4, getIdEstudiante());

            ps.execute();
            System.out.println(MConferencia.class.getName() + " edicion exitoso de inscripcion a conferencia");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean registrar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "INSERT INTO inscribirconferencia(idEstudiante,idConferencia,estado,fecha)" + "VALUES (?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getIdEstudiante());
            ps.setInt(2, getIdConferencia());
            ps.setBoolean(3, getEstado());
            ps.setDate(4, Date.valueOf(LocalDate.now()));
            ps.execute();
            System.out.println(MConferencia.class.getName() + " registro exitoso de inscripcion a conferencia");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public boolean eliminar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "DELETE FROM inscribirconferencia where idEstudiante=? and idConferencia=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getIdEstudiante());
            ps.setInt(2, getIdConferencia());
            ps.execute();
            System.out.println(MConferencia.class.getName() + " eliminado exitoso de inscripcion a conferencia");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");
        return false;
    }

    public ResultSet listar() {
        PreparedStatement ps;
        Connection con = m_Conexion.getConexion();
        String sql = "SELECT * FROM inscribirconferencia";
        ResultSet res = null;
        try {
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();
            System.out.println(MConferencia.class.getName() + " listar de inscripcion a conferencia exitosa");
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(MConferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(MConferencia.class.getName() + " ocurrio un fallo");

        return res;
    }
}//end MInscribirConferencia
