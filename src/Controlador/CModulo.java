package Controlador;

import Modelo.MModulo;
import Vista.VModulo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 24-abr-2019 01:48:03 a.m.
 */
public class CModulo implements ActionListener, MouseListener {

    public MModulo m_MModulo;
    public VModulo m_VModulo;

    public CModulo() {
        inicializar();
    }

    public void mostrar() {
        m_VModulo.mostrar();
    }

    public void registrar() {
        m_MModulo.setTitulo(m_VModulo.v_txt_titulo.getText().toString());
        m_MModulo.setDescripcion(m_VModulo.v_txt_descripcion.getText().toString());
        m_MModulo.registrar();
    }
    
    
    public void eliminar() {
       m_MModulo.setId(Integer.valueOf(m_VModulo.v_txt_id.getText().toString()));
       m_MModulo.eliminar();
    }
    
    private void editar() {
        
    }

    public void inicializar() {
        m_MModulo = new MModulo();

        m_VModulo = new VModulo();
        m_VModulo.mostrar();
        m_VModulo.setVisible(true);
        
        m_VModulo.v_btn_registrar.addActionListener(this);
//        m_VModulo.v_btn_editar.addActionListener(this);
        m_VModulo.v_btn_eliminar.addActionListener(this);

        m_VModulo.v_tb_lista_carrera.addMouseListener(this);
        m_VModulo.v_tb_lista_docente.addMouseListener(this);
        m_VModulo.v_tb_lista.addMouseListener(this);
        
   

    }

    public static void main(String[] args) {
        CModulo m_CModulo = new CModulo();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==m_VModulo.v_btn_registrar){
            registrar();
            mostrar();
        }
        if(e.getSource()==m_VModulo.v_btn_eliminar){
            eliminar();
            mostrar();
        }
        /*if(e.getSource()==m_VModulo.v_btn_editar){
            editar();
            mostrar();
        }*/
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource()==m_VModulo.v_tb_lista_carrera) {
            int fila=m_VModulo.v_tb_lista_carrera.getSelectedRow();
            m_MModulo.setIdCarrera(Integer.valueOf(m_VModulo.v_tb_lista_carrera.getValueAt(fila, 0).toString()));
            m_VModulo.v_txt_id_carrera.setText(m_VModulo.v_tb_lista_carrera.getValueAt(fila, 0).toString());
        
        }
        if (e.getSource()==m_VModulo.v_tb_lista_docente) {
            int fila=m_VModulo.v_tb_lista_docente.getSelectedRow();
            m_MModulo.setIdDocente(Integer.valueOf(m_VModulo.v_tb_lista_docente.getValueAt(fila, 0).toString()));
            m_VModulo.v_txt_id_docente.setText(m_VModulo.v_tb_lista_docente.getValueAt(fila, 0).toString());
        }
        if (e.getSource()==m_VModulo.v_tb_lista) {
            int fila=m_VModulo.v_tb_lista.getSelectedRow();
            m_MModulo.setId(Integer.valueOf(m_VModulo.v_tb_lista.getValueAt(fila, 0).toString()));
            m_VModulo.v_txt_id.setText(m_VModulo.v_tb_lista.getValueAt(fila, 0).toString());
            m_VModulo.v_txt_titulo.setText(m_VModulo.v_tb_lista.getValueAt(fila, 1).toString());
            m_VModulo.v_txt_descripcion.setText(m_VModulo.v_tb_lista.getValueAt(fila, 2).toString());
            
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    

}//end CModulo
