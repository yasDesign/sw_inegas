package Controlador;

import Modelo.MCarrera;
import Vista.VCarrera;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 05:34:26 a.m.
 */
public class CCarrera implements ActionListener, MouseListener {

    public MCarrera m_MPlanEstudio;
    public VCarrera m_VPlanEstudio;

    public CCarrera() {
        inicializar();
    }

    public void inicializar() {
        m_MPlanEstudio = new MCarrera();

        m_VPlanEstudio = new VCarrera();
        m_VPlanEstudio.mostrar();
        m_VPlanEstudio.setVisible(true);

        m_VPlanEstudio.v_btn_registrar.addActionListener(this);
        m_VPlanEstudio.v_btn_editar.addActionListener(this);
        m_VPlanEstudio.v_btn_eliminar.addActionListener(this);
        m_VPlanEstudio.v_tb_lista.addMouseListener(this);

    }

    public void registrar() {
        m_MPlanEstudio.setTitulo(m_VPlanEstudio.v_txt_titulo.getText());
        m_MPlanEstudio.setDescripcion(m_VPlanEstudio.v_txt_descripcion.getText());
        m_MPlanEstudio.setFechaInicio(m_VPlanEstudio.v_txt_fecha2.getText());
        m_MPlanEstudio.setFechaFin(m_VPlanEstudio.v_txt_fecha3.getText());
        m_MPlanEstudio.setCosto(Float.valueOf(m_VPlanEstudio.v_txt_costo.getText()));
        if (m_VPlanEstudio.v_txt_estado.getSelectedItem().toString() == "no disponible") {
            m_MPlanEstudio.setEstado(false);
        } else {
            m_MPlanEstudio.setEstado(true);
        }

        boolean res = m_MPlanEstudio.registrar();
        if (res) {
            System.out.println("controller registro exitoso");
        } else {
            System.out.println("controller registro fallido");
        }
    }

    public void eliminar() {
        m_MPlanEstudio.setId(Integer.valueOf(m_VPlanEstudio.v_txt_id.getText()));
        boolean res = m_MPlanEstudio.eliminar();
        if (res) {
            System.out.println("controller eliminacion exitoso");
        } else {
            System.out.println("controller eliminacion fallido");
        }
    }

    public void editar() {
        m_MPlanEstudio.setId(Integer.valueOf(m_VPlanEstudio.v_txt_id.getText()));
        m_MPlanEstudio.setTitulo(m_VPlanEstudio.v_txt_titulo.getText());
        m_MPlanEstudio.setDescripcion(m_VPlanEstudio.v_txt_descripcion.getText());
        m_MPlanEstudio.setFechaInicio(m_VPlanEstudio.v_txt_fecha2.getText());
        m_MPlanEstudio.setFechaFin(m_VPlanEstudio.v_txt_fecha3.getText());
        m_MPlanEstudio.setCosto(Float.valueOf(m_VPlanEstudio.v_txt_costo.getText()));
        if (m_VPlanEstudio.v_txt_estado.getSelectedItem().toString() == "no disponible") {
            m_MPlanEstudio.setEstado(false);
        } else {
            m_MPlanEstudio.setEstado(true);
        }
        boolean res = m_MPlanEstudio.editar();
        if (res) {
            System.out.println("controller edicion exitoso");
        } else {
            System.out.println("controller edicion fallido");
        }
    }

    public static void main(String[] args) {
        CCarrera m_CPlanEstudio = new CCarrera();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("nro table " + m_VPlanEstudio.v_tb_lista.getSelectedRow());
        int fila = m_VPlanEstudio.v_tb_lista.getSelectedRow();
        m_VPlanEstudio.v_txt_id.setText((String) m_VPlanEstudio.v_tb_lista.getValueAt(fila, 0));
        m_VPlanEstudio.v_txt_titulo.setText((String) m_VPlanEstudio.v_tb_lista.getValueAt(fila, 1));
        m_VPlanEstudio.v_txt_descripcion.setText((String) m_VPlanEstudio.v_tb_lista.getValueAt(fila, 2));
        
        m_VPlanEstudio.v_txt_fecha2.setText((String) m_VPlanEstudio.v_tb_lista.getValueAt(fila, 3));
        m_VPlanEstudio.v_txt_fecha3.setText((String) m_VPlanEstudio.v_tb_lista.getValueAt(fila, 4));
        m_VPlanEstudio.v_txt_costo.setText((String) m_VPlanEstudio.v_tb_lista.getValueAt(fila, 5));
        m_VPlanEstudio.v_txt_estado.setSelectedItem(m_VPlanEstudio.v_tb_lista.getValueAt(fila, 6));
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VPlanEstudio.v_btn_registrar) {
            registrar();
            mostrar();
            System.out.println("asdasd asdasd click button registrar");
        }
        if (e.getSource() == m_VPlanEstudio.v_btn_editar) {
            editar();
            mostrar();
            System.out.println("asdasd asdasd click button editar");
        }
        if (e.getSource() == m_VPlanEstudio.v_btn_eliminar) {
            eliminar();
            mostrar();
            System.out.println("asdasd asdasd click button eliminar");
        }
    }

    private void mostrar() {
        m_VPlanEstudio.mostrar();
    }

}//end CPlanEstudio
