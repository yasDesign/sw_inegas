package Controlador;

import Modelo.MDocente;
import Vista.VDocente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 04:54:11 a.m.
 */
public class CDocente implements ActionListener, MouseListener {

    public MDocente m_MDocente;
    public VDocente m_VDocente;

    public CDocente() {
        inicializar();
    }

    public void inicializar() {
        m_VDocente = new VDocente();
        m_VDocente.mostrar();
        m_VDocente.setVisible(true);
        

        //se cnfigura para escuchar los eventos click,seleccionar item tabla
        //eventos de los botones
        m_VDocente.v_btn_registrar.addActionListener(this);
        m_VDocente.v_btn_editar.addActionListener(this);
        m_VDocente.v_btn_eliminar.addActionListener(this);
        //eventos de los tabla
        m_VDocente.v_tb_lista.addMouseListener(this);

        m_MDocente = new MDocente();
    }

    public void mostrar() {
        m_VDocente.mostrar();
    }

    public void registrar() {
        m_MDocente.setCi(Integer.valueOf(m_VDocente.v_txt_ci.getText()));
        m_MDocente.setCorreo(m_VDocente.v_txt_correo.getText());
        m_MDocente.setDireccion(m_VDocente.v_txt_direccion.getText());
        m_MDocente.setFechaNac(m_VDocente.v_txt_fechaNac.getText());
        m_MDocente.setNombre(m_VDocente.v_txt_nombre.getText());
        m_MDocente.setProfesion(m_VDocente.v_txt_preofesion.getText());
        m_MDocente.setTelefono(Integer.valueOf(m_VDocente.v_txt_telefono.getText()));
        boolean res = m_MDocente.registrar();
        if (res) {
            System.out.println("controller registro exitoso");
        } else {
            System.out.println("controller registro fallido");
        }
    }

    public void eliminar() {
        m_MDocente.setId(Integer.valueOf(m_VDocente.v_txt_id.getText()));
        boolean res = m_MDocente.eliminar();
        if (res) {
            System.out.println("controller eliminacion exitoso");
        } else {
            System.out.println("controller eliminacion fallido");
        }
    }

    public void editar() {
        m_MDocente.setId(Integer.valueOf(m_VDocente.v_txt_id.getText()));
        m_MDocente.setCi(Integer.valueOf(m_VDocente.v_txt_ci.getText()));
        m_MDocente.setCorreo(m_VDocente.v_txt_correo.getText());
        m_MDocente.setDireccion(m_VDocente.v_txt_direccion.getText());
        m_MDocente.setFechaNac(m_VDocente.v_txt_fechaNac.getText());
        m_MDocente.setNombre(m_VDocente.v_txt_nombre.getText());
        m_MDocente.setProfesion(m_VDocente.v_txt_preofesion.getText());
        m_MDocente.setTelefono(Integer.valueOf(m_VDocente.v_txt_telefono.getText()));
        boolean res = m_MDocente.editar();
        if (res) {
            System.out.println("controller edicion exitoso");
        } else {
            System.out.println("controller edicion fallido");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VDocente.v_btn_registrar) {
            registrar();
            mostrar();
            System.out.println("asdasd asdasd click button registrar");
        }
        if (e.getSource() == m_VDocente.v_btn_editar) {
            editar();
            mostrar();
            System.out.println("asdasSd asdasd click button editar");
        }
        if (e.getSource() == m_VDocente.v_btn_eliminar) {
            eliminar();
            mostrar();
            System.out.println("asdasd asdasd click button eliminar");
        }
    }

    public static void main(String[] args) {
        CDocente m = new CDocente();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("nro table " + m_VDocente.v_tb_lista.getSelectedRow());
        int fila = m_VDocente.v_tb_lista.getSelectedRow();
        m_VDocente.v_txt_nombre.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 1));
        m_VDocente.v_txt_ci.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 2));
        m_VDocente.v_txt_correo.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 7));

        m_VDocente.v_txt_direccion.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 5));
        m_VDocente.v_txt_fechaNac.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 4));

        m_VDocente.v_txt_id.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 0));

        m_VDocente.v_txt_preofesion.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 3));
        m_VDocente.v_txt_telefono.setText((String) m_VDocente.v_tb_lista.getValueAt(fila, 6));

    }

    @Override
    public void mousePressed(MouseEvent e) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}//end CDocente
