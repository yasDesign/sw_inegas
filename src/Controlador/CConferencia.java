package Controlador;

import Modelo.MConferencia;
import Vista.VConferencia;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 05:34:53 a.m.
 */
public class CConferencia implements ActionListener, MouseListener {

    private VConferencia m_VConferencia;
    private MConferencia m_MConferencia;

    public CConferencia() {
        inicializar();
    }

    public void inicializar() {
        m_MConferencia = new MConferencia();

        m_VConferencia = new VConferencia();
        m_VConferencia.mostrar();
        m_VConferencia.setVisible(true);
        m_VConferencia.v_btn_registrar.addActionListener(this);
        m_VConferencia.v_btn_editar.addActionListener(this);
        m_VConferencia.v_btn_eliminar.addActionListener(this);
        m_VConferencia.v_tb_lista.addMouseListener(this);

    }
    
            
    public void registrar() {
        m_MConferencia.setTitulo(m_VConferencia.v_txt_titulo.getText());
        m_MConferencia.setDescripcion(m_VConferencia.v_txt_descripcion.getText());
        m_MConferencia.setExpositor(m_VConferencia.v_txt_expositor.getText());
        m_MConferencia.setInfoExpositor(m_VConferencia.v_txt_infoExpositor.getText());
        m_MConferencia.setFecha(m_VConferencia.v_txt_fecha.getText());
        m_MConferencia.setHora(m_VConferencia.v_txt_hora.getText());
        boolean res = m_MConferencia.registrar();
        if (res) {
            System.out.println("controller registro exitoso");
        } else {
            System.out.println("controller registro fallido");
        }
    }

    public void eliminar() {
        m_MConferencia.setId(Integer.valueOf(m_VConferencia.v_txt_id.getText()));
        boolean res = m_MConferencia.eliminar();
        if (res) {
            System.out.println("controller eliminacion exitoso");
        } else {
            System.out.println("controller eliminacion fallido");
        }
    }

    public void editar() {
        m_MConferencia.setId(Integer.valueOf(m_VConferencia.v_txt_id.getText()));
        m_MConferencia.setTitulo(m_VConferencia.v_txt_titulo.getText());
        m_MConferencia.setDescripcion(m_VConferencia.v_txt_descripcion.getText());
        m_MConferencia.setExpositor(m_VConferencia.v_txt_expositor.getText());
        m_MConferencia.setInfoExpositor(m_VConferencia.v_txt_infoExpositor.getText());
        m_MConferencia.setFecha(m_VConferencia.v_txt_fecha.getText());
        m_MConferencia.setHora(m_VConferencia.v_txt_hora.getText());
        boolean res = m_MConferencia.editar();
        if (res) {
            System.out.println("controller edicion exitoso");
        } else {
            System.out.println("controller edicionfallido");
        }
    }

    public void mostrar() {
        m_VConferencia.mostrar();
    }

    public static void main(String[] args) {
        CConferencia m_CConferencia = new CConferencia();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VConferencia.v_btn_registrar) {
            registrar();
            mostrar();
            System.out.println("asdasd asdasd click button registrar");
        }
        if (e.getSource() == m_VConferencia.v_btn_editar) {
            editar();
            mostrar();
            System.out.println("asdasd asdasd click button editar");
        }
        if (e.getSource() == m_VConferencia.v_btn_eliminar) {
            eliminar();
            mostrar();
            System.out.println("asdasd asdasd click button eliminar");
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("nro table " + m_VConferencia.v_tb_lista.getSelectedRow());
        int fila = m_VConferencia.v_tb_lista.getSelectedRow();
        m_VConferencia.v_txt_titulo.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 1));
        m_VConferencia.v_txt_descripcion.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 2));
        m_VConferencia.v_txt_expositor.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 3));

        m_VConferencia.v_txt_infoExpositor.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 4));
        m_VConferencia.v_txt_fecha.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 5));
        m_VConferencia.v_txt_hora.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 6));

        m_VConferencia.v_txt_id.setText((String) m_VConferencia.v_tb_lista.getValueAt(fila, 0));
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}//end CConferencia
