package Controlador;

import Modelo.MEstudiante;
import Vista.VEstudiante;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 20-abr-2019 05:34:42 a.m.
 */
public class CEstudiante implements ActionListener, MouseListener {

    public MEstudiante m_MEstudiante;
    public VEstudiante m_VEstudiante;

    public CEstudiante() {
        inicializar();
    }

    public void inicializar() {
        m_MEstudiante = new MEstudiante();

        m_VEstudiante = new VEstudiante();
        m_VEstudiante.mostrar();
        m_VEstudiante.setVisible(true);

        m_VEstudiante.v_btn_registrar.addActionListener(this);
        m_VEstudiante.v_btn_editar.addActionListener(this);
        m_VEstudiante.v_btn_eliminar.addActionListener(this);

        m_VEstudiante.v_tb_lista.addMouseListener(this);

    }

    public void registrar() {

        m_MEstudiante.setCorreo(m_VEstudiante.v_txt_correo.getText());
        m_MEstudiante.setCi(Integer.valueOf(m_VEstudiante.v_txt_ci.getText()));
        m_MEstudiante.setFechaNac(m_VEstudiante.v_txt_fechaNac.getText());
        m_MEstudiante.setNombre(m_VEstudiante.v_txt_nombre.getText());
        m_MEstudiante.setProfesion(m_VEstudiante.v_txt_preofesion.getText());
        m_MEstudiante.setTelefono(Integer.valueOf(m_VEstudiante.v_txt_telefono.getText()));
        boolean res = m_MEstudiante.registrar();
        if (res) {
            System.out.println("controller registro exitoso");
        } else {
            System.out.println("controller registro fallido");
        }
    }

    public void eliminar() {
        m_MEstudiante.setId(Integer.valueOf(m_VEstudiante.v_txt_id.getText()));
        boolean res = m_MEstudiante.eliminar();
        if (res) {
            System.out.println("controller eliminacion exitoso");
        } else {
            System.out.println("controller eliminacion fallido");
        }
    }

    public void editar() {
        m_MEstudiante.setId(Integer.valueOf(m_VEstudiante.v_txt_id.getText()));
        m_MEstudiante.setCi(Integer.valueOf(m_VEstudiante.v_txt_ci.getText()));
        m_MEstudiante.setCorreo(m_VEstudiante.v_txt_correo.getText());
        m_MEstudiante.setFechaNac(m_VEstudiante.v_txt_fechaNac.getText());
        m_MEstudiante.setNombre(m_VEstudiante.v_txt_nombre.getText());
        m_MEstudiante.setProfesion(m_VEstudiante.v_txt_preofesion.getText());
        m_MEstudiante.setTelefono(Integer.valueOf(m_VEstudiante.v_txt_telefono.getText()));
        boolean res = m_MEstudiante.editar();
        if (res) {
            System.out.println("controller edicion exitoso");
        } else {
            System.out.println("controller edicion fallido");
        }
    }

    public static void main(String[] args) {
        CEstudiante m_CEstudiante = new CEstudiante();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VEstudiante.v_btn_registrar) {
            registrar();
            mostrar();
            System.out.println("asdasd asdasd click button registrar");
        }
        if (e.getSource() == m_VEstudiante.v_btn_editar) {
            editar();
            mostrar();
            System.out.println("asdasd asdasd click button editar");
        }
        if (e.getSource() == m_VEstudiante.v_btn_eliminar) {
            eliminar();
            mostrar();
            System.out.println("asdasd asdasd click button eliminar");
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("nro table " + m_VEstudiante.v_tb_lista.getSelectedRow());
        int fila = m_VEstudiante.v_tb_lista.getSelectedRow();
        m_VEstudiante.v_txt_id.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 0));
        m_VEstudiante.v_txt_nombre.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 1));
        m_VEstudiante.v_txt_correo.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 2));
        m_VEstudiante.v_txt_ci.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 3));
        m_VEstudiante.v_txt_telefono.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 4));
        m_VEstudiante.v_txt_fechaNac.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 5));
        m_VEstudiante.v_txt_preofesion.setText((String) m_VEstudiante.v_tb_lista.getValueAt(fila, 6));

    }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void mostrar() {
        m_VEstudiante.mostrar();
    }

}//end CEstudiante
