package Controlador;

import Vista.VInscribirModulo;
import Modelo.MDetalleModulo;
import Modelo.MInscribirModulo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 24-abr-2019 06:47:56 a.m.
 */
public class CInscribirModulo implements ActionListener, MouseListener {

    public VInscribirModulo m_VInscribirModulo;
    public MDetalleModulo m_MDetalleModulo;
    public MInscribirModulo m_MInscribirModulo;

    public CInscribirModulo(){
        inicializar();
    }
    
    public void inicializar() {
        m_MDetalleModulo = new MDetalleModulo();
        m_MInscribirModulo = new MInscribirModulo();

        m_VInscribirModulo = new VInscribirModulo();
        m_VInscribirModulo.mostrar();
        m_VInscribirModulo.setVisible(true);
        m_VInscribirModulo.v_btn_registrar.addActionListener(this);
        m_VInscribirModulo.v_btn_eliminar.addActionListener(this);

        m_VInscribirModulo.v_tb_lista_estudiante.addMouseListener(this);
        m_VInscribirModulo.v_tb_lista_modulo.addMouseListener(this);
        m_VInscribirModulo.v_tb_lista.addMouseListener(this);
    }

    public void registrar() {
        m_MInscribirModulo.setCodigo(Integer.valueOf(m_VInscribirModulo.v_txt_id_codigo.getText()));
        int idfact=m_MInscribirModulo.registrar();
        System.out.println("id fact "+idfact);
        m_MDetalleModulo.setIdInscripcionModulo(idfact);
        m_MDetalleModulo.registrar();
        System.out.println("registro exitosos en .. modulo..");
    }

    public void eliminar() {
        m_MDetalleModulo.eliminar();
        m_MInscribirModulo.eliminar();
    }

    public void mostrar() {
        m_VInscribirModulo.mostrar();
    }

    public static void main(String[] args) {
        CInscribirModulo m_CInscribirModulo = new CInscribirModulo();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VInscribirModulo.v_btn_registrar) {
            registrar();
            mostrar();
            System.out.println("registran.. action performed");
        }
        if (e.getSource() == m_VInscribirModulo.v_btn_eliminar) {
            eliminar();
            System.out.println("elimiar.. action performed");
            mostrar();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == m_VInscribirModulo.v_tb_lista_estudiante) {
            int fila = m_VInscribirModulo.v_tb_lista_estudiante.getSelectedRow();
            int idEst = Integer.valueOf(m_VInscribirModulo.v_tb_lista_estudiante.getValueAt(fila, 0).toString());
            m_VInscribirModulo.v_txt_id_estudiante.setText(String.valueOf(idEst));
            m_MInscribirModulo.setIdEstudiante(idEst);
        }
        if (e.getSource() == m_VInscribirModulo.v_tb_lista_modulo) {
            int fila = m_VInscribirModulo.v_tb_lista_modulo.getSelectedRow();
            int idEst=Integer.valueOf(m_VInscribirModulo.v_tb_lista_modulo.getValueAt(fila, 0).toString());
            m_MDetalleModulo.setIdModulo(idEst);
            m_VInscribirModulo.v_txt_id_modulo.setText(String.valueOf(idEst));
        }
        if (e.getSource() == m_VInscribirModulo.v_tb_lista) {
            int fila = m_VInscribirModulo.v_tb_lista.getSelectedRow();
            int idEst=Integer.valueOf(m_VInscribirModulo.v_tb_lista.getValueAt(fila, 0).toString());
            m_MDetalleModulo.setIdInscripcionModulo(idEst);
            m_MInscribirModulo.setId(idEst);
            m_VInscribirModulo.v_txt_id.setText(String.valueOf(idEst));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}//end CInscribirModulo
