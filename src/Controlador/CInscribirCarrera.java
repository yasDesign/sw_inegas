package Controlador;

import Modelo.MDetalleCarrera;
import Modelo.MInscribirCarrera;
import Vista.VInscribirCarrrera;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 21-abr-2019 10:12:37 p.m.
 */
public class CInscribirCarrera implements ActionListener, MouseListener {

    public MDetalleCarrera m_MDetalleCarrera;
    public MInscribirCarrera m_MInscribirCarrera;
    public VInscribirCarrrera m_VInscribirCarrera;

    public CInscribirCarrera() {
        inicializar();
    }

    public void inicializar() {
        m_MDetalleCarrera = new MDetalleCarrera();
        m_MInscribirCarrera = new MInscribirCarrera();

        m_VInscribirCarrera = new VInscribirCarrrera();
        m_VInscribirCarrera.mostrar();
        m_VInscribirCarrera.setVisible(true);
        m_VInscribirCarrera.v_btn_registrar.addActionListener(this);
        m_VInscribirCarrera.v_btn_eliminar.addActionListener(this);

        m_VInscribirCarrera.v_tb_lista_estudiante.addMouseListener(this);
        m_VInscribirCarrera.v_tb_lista_planEstudio.addMouseListener(this);
        m_VInscribirCarrera.v_tb_lista.addMouseListener(this);
        
    }

    public void registrar() {
        //registro inscriocni plant estudio
        m_MInscribirCarrera.setCodigo(Integer.valueOf(m_VInscribirCarrera.v_txt_id_codigo.getText()));
        int idFac = m_MInscribirCarrera.registrar();
        System.out.println("id factura " + idFac);
        //registro de detalle plant estudio
        m_MDetalleCarrera.setIdInscripcionCarrera(idFac);
        int[] listaPlan = m_VInscribirCarrera.v_tb_lista_planEstudio.getSelectedRows();
        for (int i = 0; i < listaPlan.length; i++) {
            m_MDetalleCarrera.setIdCarrera(Integer.valueOf(m_VInscribirCarrera.v_tb_lista_planEstudio.getValueAt(listaPlan[i], 0).toString()));
            m_MDetalleCarrera.registrar();
        }
        mostrar();

    }

    public void eliminar() {
        //eliminar de detalle plant estudio
        //m_MDetalleCarrera.setIdInscripcionCarrera(m_MInscribirCarrera.getId());
        m_MDetalleCarrera.eliminar();
        //registro inscriocni plant estudio
        m_MInscribirCarrera.eliminar();
        System.out.println("elimianr id factura ");

    }

    public void mostrar() {
        m_VInscribirCarrera.mostrar();
    }

    public static void main(String[] args) {
        CInscribirCarrera m_CInscribirCarrera = new CInscribirCarrera();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VInscribirCarrera.v_btn_registrar) {
            registrar();

            System.out.println("registran.. action performed");
        }
        if (e.getSource() == m_VInscribirCarrera.v_btn_eliminar) {
            eliminar();
            System.out.println("elimiar.. action performed");
            mostrar();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == m_VInscribirCarrera.v_tb_lista_estudiante) {
            int fila = m_VInscribirCarrera.v_tb_lista_estudiante.getSelectedRow();
            int idEst = Integer.valueOf(m_VInscribirCarrera.v_tb_lista_estudiante.getValueAt(fila, 0).toString());
            m_VInscribirCarrera.v_txt_id_estudiante.setText(String.valueOf(idEst));
            m_MInscribirCarrera.setIdEstudiante(idEst);
        }
        if (e.getSource() == m_VInscribirCarrera.v_tb_lista_planEstudio) {
            int[] fila = m_VInscribirCarrera.v_tb_lista_planEstudio.getSelectedRows();
            //int idEst=Integer.valueOf(m_VInscribirCarrera.v_tb_lista_planEstudio.getValueAt(fila, 0).toString());
            String ids = "";
            for (int i = 0; i < fila.length; i++) {
                ids = ids + m_VInscribirCarrera.v_tb_lista_planEstudio.getValueAt(fila[i], 0).toString() + ",";
            }
            m_VInscribirCarrera.v_txt_id_planEstudio.setText(String.valueOf(ids));
        }
        if (e.getSource() == m_VInscribirCarrera.v_tb_lista) {
            int fila = m_VInscribirCarrera.v_tb_lista.getSelectedRow();
            int idEst = Integer.valueOf(m_VInscribirCarrera.v_tb_lista.getValueAt(fila, 1).toString());
            m_VInscribirCarrera.v_txt_id_inscripcion.setText(String.valueOf(idEst));
            //m_MInscribirCarrera.setId(idEst);
            m_MInscribirCarrera.setId(idEst);
            m_MDetalleCarrera.setIdInscripcionCarrera(idEst);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}//end CinscribirCarrera
