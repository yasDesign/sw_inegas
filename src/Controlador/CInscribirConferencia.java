package Controlador;

import Vista.VInscribirConferencia;
import Modelo.MEstudiante;
import Modelo.MConferencia;
import Modelo.MInscribirConferencia;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author yas
 * @version 1.0
 * @created 21-abr-2019 03:25:11 p.m.
 */
public class CInscribirConferencia implements ActionListener, MouseListener {

    VInscribirConferencia m_VInscribirConferencia;
    MInscribirConferencia m_MInscribirConferencia;

    public CInscribirConferencia() {
        inicializar();
    }

    public void inicializar() {
        m_MInscribirConferencia = new MInscribirConferencia();

        m_VInscribirConferencia = new VInscribirConferencia();
        m_VInscribirConferencia.mostrar();
        m_VInscribirConferencia.setVisible(true);

        m_VInscribirConferencia.v_btn_registrar.addActionListener(this);
        //m_VInscribirConferencia.v_btn_editar.addActionListener(this);
        m_VInscribirConferencia.v_btn_eliminar.addActionListener(this);
        m_VInscribirConferencia.v_tb_lista.addMouseListener(this);
        m_VInscribirConferencia.v_tb_lista_conferencia.addMouseListener(this);
        m_VInscribirConferencia.v_tb_lista_estudiante.addMouseListener(this);
    }

    public void mostrar() {
        m_VInscribirConferencia.mostrar();
    }

    public void registrar() {

        if (m_VInscribirConferencia.v_cb_estado.getSelectedItem().toString() == "habilitado") {
            m_MInscribirConferencia.setEstado(true);
        } else {
            m_MInscribirConferencia.setEstado(false);
        }
        boolean res = m_MInscribirConferencia.registrar();
        if (res) {
            System.out.println("controller registro exitoso");
        } else {
            System.out.println("controller registro fallido");
        }

    }

    public void eliminar() {

        m_MInscribirConferencia.setIdConferencia(Integer.valueOf(m_VInscribirConferencia.v_txt_id_conferencia.getText()));
        m_MInscribirConferencia.setIdEstudiante(Integer.valueOf(m_VInscribirConferencia.v_txt_id_estudiante.getText()));

        boolean res = m_MInscribirConferencia.eliminar();
        if (res) {
            System.out.println("controller eliminacion exitoso");
        } else {
            System.out.println("controller eliminacion fallido");
        }

    }

    public void editar() {
        System.out.println("estaod.... " + m_VInscribirConferencia.v_cb_estado.getSelectedItem().toString());
        if (m_VInscribirConferencia.v_cb_estado.getSelectedItem().toString() == "habilitado") {
            m_MInscribirConferencia.setEstado(true);
        } else {
            m_MInscribirConferencia.setEstado(false);
        }
        boolean res = m_MInscribirConferencia.editar();
        if (res) {
            System.out.println("controller editar exitoso");
        } else {
            System.out.println("controller editar fallido");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == m_VInscribirConferencia.v_btn_registrar) {
            registrar();
            mostrar();
            System.out.println("asdasd asdasd click button registrar");
        }
        /*if (e.getSource() == m_VInscribirConferencia.v_btn_editar) {
            editar();
            mostrar();
            System.out.println("asdasd asdasd click button editar");
        }*/
        if (e.getSource() == m_VInscribirConferencia.v_btn_eliminar) {
            eliminar();
            mostrar();
            System.out.println("asdasd asdasd click button eliminar");
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getSource() == m_VInscribirConferencia.v_tb_lista_conferencia) {
            int fila = m_VInscribirConferencia.v_tb_lista_conferencia.getSelectedRow();
            int idconf = Integer.valueOf(m_VInscribirConferencia.v_tb_lista_conferencia.getValueAt(fila, 0).toString());
            m_MInscribirConferencia.setIdConferencia(idconf);
            System.out.println("idconference:" + idconf);
        }
        if (e.getSource() == m_VInscribirConferencia.v_tb_lista_estudiante) {
            int fila = m_VInscribirConferencia.v_tb_lista_estudiante.getSelectedRow();
            int idest = Integer.valueOf(m_VInscribirConferencia.v_tb_lista_estudiante.getValueAt(fila, 0).toString());
            m_MInscribirConferencia.setIdEstudiante(idest);
            System.out.println("idESTUDOE:" + idest);

        }
        if (e.getSource() == m_VInscribirConferencia.v_tb_lista) {
            int fila = m_VInscribirConferencia.v_tb_lista.getSelectedRow();

            ///SELECCIONAR LOS OTROS TABLAS
            int idest = Integer.valueOf(m_VInscribirConferencia.v_tb_lista.getValueAt(fila, 0).toString());
            int idconf = Integer.valueOf(m_VInscribirConferencia.v_tb_lista.getValueAt(fila, 1).toString());

            m_VInscribirConferencia.v_txt_id_estudiante.setText(String.valueOf(idest));
            m_VInscribirConferencia.v_txt_id_conferencia.setText(String.valueOf(idconf));
            if (m_VInscribirConferencia.v_tb_lista.getValueAt(fila, 3).toString() == "habilitado") {
                m_VInscribirConferencia.v_cb_estado.setSelectedIndex(0);
            } else {
                m_VInscribirConferencia.v_cb_estado.setSelectedIndex(1);
            }

        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public static void main(String[] args) {
        CInscribirConferencia m_CInscribirConferencia = new CInscribirConferencia();
    }
}//end CInscribirConferencia
